/*
	DayZ Mission System Functions by AmberHelios 2014
*/
diag_log text "[AmberMissions]: loading execVM sqf scripts.";
AmberMissionsMajTimer = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsMajTimer.sqf";
AmberMissionsMinTimer = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsMinTimer.sqf";
AmberMissionsMarkerLoop = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsMarkerLoop.sqf";

AmberMissionsAddMajMarker = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsAddMajMarker.sqf";
AmberMissionsAddMinMarker = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsAddMinMarker.sqf";

AmberMissionsAIKilled = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsAIKilled.sqf";

AmberMissionsBoxSetup = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsBox.sqf";
AmberMissionsSaveVeh = "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsSaveToHive.sqf";

diag_log text "[AmberMissions]: loading compiled functions.";
AmberMissionsAISpawn = compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsAISpawn.sqf";
diag_log text "[AmberMissions]: loading all other functions.";
AmberMissionsFindPos = {
     private["_mapHardCenter","_mapRadii","_isTavi","_centerPos","_pos","_hardX","_hardY","_findRun","_posX","_posY","_feel1","_feel2","_feel3","_feel4","_noWater","_tavTest","_tavHeight","_disMaj","_disMin","_okDis","_isBlack"];
  
	_mapHardCenter = true;
	_isTavi = false;
	switch (AmberMissionsWorldName) do {
		case "chernarus":{_centerPos = [7100, 7750, 0];_mapRadii = 5500;};
		case "utes":{_centerPos = [3500, 3500, 0];_mapRadii = 3500;};
		case "zargabad":{_centerPos = [4096, 4096, 0];_mapRadii = 4096;};
		case "fallujah":{_centerPos = [3500, 3500, 0];_mapRadii = 3500;};
		case "takistan":{_centerPos = [8000, 1900, 0]};
		case "tavi":{_centerPos = [10370, 11510, 0];_mapRadii = 14090;_isTavi = true;};
		case "lingor":{_centerPos = [4400, 4400, 0];_mapRadii = 4400;};
		case "namalsk":{_centerPos = [4352, 7348, 0]};
		case "napf":{_centerPos = [10240, 10240, 0];_mapRadii = 10240;};
		case "mbg_celle2":{_centerPos = [8765.27, 2075.58, 0]};
		case "oring":{_centerPos = [1577, 3429, 0]};
		case "panthera2":{_centerPos = [4400, 4400, 0];_mapRadii = 4400;};
		case "isladuala":{_centerPos = [4400, 4400, 0];_mapRadii = 4400;};
		case "smd_sahrani_a2":{_centerPos = [13200, 8850, 0]};
		case "sauerland":{_centerPos = [12800, 12800, 0];_mapRadii = 12800;};
		case "trinity":{_centerPos = [6400, 6400, 0];_mapRadii = 6400;};
		default{_pos = [getMarkerPos "center",0,5500,60,0,20,0] call BIS_fnc_findSafePos;_mapHardCenter = false;};
	};
    if (_mapHardCenter) then {
   
        _hardX = _centerPos select 0;
        _hardY = _centerPos select 1;

        _findRun = true;
        while {_findRun} do
        {
            _pos = [_centerPos,0,_mapRadii,60,0,20,0] call BIS_fnc_findSafePos;
            _posX = _pos select 0;
            _posY = _pos select 1;
            _feel1 = [_posX, _posY+50, 0];
            _feel2 = [_posX+50, _posY, 0];
            _feel3 = [_posX, _posY-50, 0];
            _feel4 = [_posX-50, _posY, 0];

            _noWater = (!surfaceIsWater _pos && !surfaceIsWater _feel1 && !surfaceIsWater _feel2 && !surfaceIsWater _feel3 && !surfaceIsWater _feel4);

			if (_isTavi) then {
				_tavTest = createVehicle ["Can_Small",[_posX,_posY,0],[], 0, "CAN_COLLIDE"];
				_tavHeight = (getPosASL _tavTest) select 2;
				deleteVehicle _tavTest;
			};
           //Lets check for minimum mission separation distance
 			_disMaj = (_pos distance AmberMissionsMajCoords);
			_disMin = (_pos distance AmberMissionsMinCoords);
 			_okDis = ((_disMaj > 1000) AND (_disMin > 1000));
			_isBlack = false;
             {
             if ((_pos distance (_x select 0)) <= (_x select 1)) then {_isBlack = true;};
             } forEach AmberMissionsBlacklistZones;
 
            if ((_posX != _hardX) AND (_posY != _hardY) AND _noWater AND _okDis AND !_isBlack) then {
				if (!(_isTavi)) then {
					_findRun = false;
				};
				if (_isTavi AND (_tavHeight <= 185)) then {
					_findRun = false;
				};
            };
			//diag_log text format ["[AmberMissions]: DEBUG: Pos:[%1,%2] / noWater?:%3 / okDistance?:%4 / TaviHeight:%5 / BlackListed?:%6", _posX, _posY, _noWater, _okDis, _tavHeight, _isBlack];
            sleep 2;
        }; 
    };
    _fin = [(_pos select 0), (_pos select 1), 0];
    _fin
};
//Clears the cargo and sets fuel, Adds ID for trader
AmberMissionsSetupVehicle = {
	private ["_object","_objectID","_ranFuel"];
	_object = _this select 0;

	_objectID = str(round(random 999999));
	_object setVariable ["ObjectID", _objectID, true];
	_object setVariable ["ObjectUID", _objectID, true];
	_object setVariable ["DZAI",1,true];
	
	if (AmberMissionsEpoch) then {
		PVDZE_serverObjectMonitor set [count PVDZE_serverObjectMonitor, _object];
	} else {
		dayz_serverObjectMonitor set [count dayz_serverObjectMonitor, _object];
	};
	waitUntil {(!isNull _object)};
	clearWeaponCargoGlobal _object;
	clearMagazineCargoGlobal _object;
	_ranFuel = random 1;
	if (_ranFuel < .1) then {_ranFuel = .1;};
	_object setFuel _ranFuel;
	_object setvelocity [0,0,1];
	_object setDir (round(random 360));
	//If saving vehicles to the database is disabled warn the players it will disappear
	if (!(AmberMissionsSaveVehicles)) then {
		_object addEventHandler ["GetIn",{
			_nil = [nil,(_this select 2),"loc",rTITLETEXT,"Warning: This vehicle will disappear on server restart!","PLAIN DOWN",5] call RE;
		}];
	};
	true
};
//Prevents an object being cleaned up by the server anti-hack
AmberMissionsProtectObj = {
	private ["_object","_objectID"];
	_object = _this select 0;
	_objectID = str(round(random 999999));
	_object setVariable ["ObjectID", _objectID, true];
	_object setVariable ["ObjectUID", _objectID, true];
	
	if (_object isKindOf "ReammoBox") then {
		_object setVariable ["permaLoot",true];
	};

	if (AmberMissionsEpoch) then {
		PVDZE_serverObjectMonitor set [count PVDZE_serverObjectMonitor, _object];
	} else {
		dayz_serverObjectMonitor set [count dayz_serverObjectMonitor, _object];
	};

    if (!((typeOf _object) in ["USVehicleBox","USLaunchersBox","AmmoBoxSmall_556","AmmoBoxSmall_762","MedBox0","USBasicWeaponsBox","USBasicAmmunitionBox","RULaunchersBox"]) || AmberMissionsSceneryDespawnLoot) then {
        _object setVariable["AmberMissionsCleanup",true];
    };
	true
};
//Gets the weapon and magazine based on skill level
AmberMissionsGetWeapon = {
	private ["_skill","_aiweapon","_weapon","_magazine","_fin"];
	_skill = _this select 0;
	
	//diag_log text format ["[AmberMissions]: AI Skill Func:%1",_skill];
	
	switch (_skill) do {
		case 0: {_aiweapon = AmberMissionsWeps1;};
		case 1: {_aiweapon = AmberMissionsWeps2;};
		case 2: {_aiweapon = AmberMissionsWeps3;};
		case 3: {_aiweapon = AmberMissionsWeps4;}; // Snipers Only
		case 4: {_aiweapon = AmberMissionsWeps5;};
		case 5: {_aiweapon = AmberMissionsWeps5;};
		case 6: {_aiweapon = AmberMissionsWeps5;};
	};
	if (_skill <= 4) then {
		_weapon = _aiweapon call BIS_fnc_selectRandom;
		_magazine = getArray (configFile >> "CfgWeapons" >> _weapon >> "magazines") select 0;
	} else {
		_weapon = "MeleeHatchet_DZE";
		_magazine = "5Rnd_762x51_M24";
		};
		
	_fin = [_weapon,_magazine];
	
	_fin
};
AmberMissionsWaitMissionComp = {
    private["_objective","_unitArrayName","_numSpawned","_numKillReq"];
    _objective = _this select 0;
    _unitArrayName = _this select 1;
    call compile format["_numSpawned = count %1;",_unitArrayName];
    _numKillReq = ceil(AmberMissionsRequiredKillPercent * _numSpawned);
    diag_log text format ["[AmberMissions]: (%3) waiting for %1/%2 units or less to be alive and a player to be near objective.",(_numSpawned - _numKillReq),_numSpawned,_unitArrayName];
    call compile format["waitUntil{sleep 1; ({isPlayer _x && _x distance _objective <= 30} count playableUnits > 0) && ({alive _x} count %1 <= (_numSpawned - _numKillReq));};",_unitArrayName];
    if (AmberMissionsSceneryDespawnTimer > 0) then {_objective spawn AmberMissionsCleanupThread;};
};
AmberMissionsSleep = {
    private["_sleepTime","_checkInterval","_startTime"];
    _sleepTime = _this select 0;
    _checkInterval = _this select 1;
    _startTime = diag_tickTime;
    waitUntil{sleep _checkInterval; (diag_tickTime - _startTime) > _sleepTime;};
};
AmberMissionsPurgeObject = {
    _this enableSimulation false;
    _this removeAllMPEventHandlers "mpkilled";
    _this removeAllMPEventHandlers "mphit";
    _this removeAllMPEventHandlers "mprespawn";
    _this removeAllEventHandlers "FiredNear";
    _this removeAllEventHandlers "HandleDamage";
    _this removeAllEventHandlers "Killed";
    _this removeAllEventHandlers "Fired";
    _this removeAllEventHandlers "GetOut";
    _this removeAllEventHandlers "GetIn";
    _this removeAllEventHandlers "Local";
    clearVehicleInit _this;
    deleteVehicle _this;
    deleteGroup (group _this);
    _this = nil;
};
AmberMissionsCleanupThread = {
    [AmberMissionsSceneryDespawnTimer,20] call AmberMissionsSleep;
    {
        if (_x getVariable ["AmberMissionsCleanup",false]) then {
            _x call AmberMissionsPurgeObject;
        };
    } forEach (_this nearObjects 50);
};
diag_log text format ["[AmberMissions]: Mission Functions Script Loaded!"];