/*
	AmberMissionsInit.sqf by AmberHelios 2014
*/
private["_modVariant"];

waitUntil{initialized};

sleep 60;

// Error Check
if (!isServer) exitWith { diag_log text format ["[AmberMissions]: <ERROR> AmberMissions is Installed Incorrectly! AmberMissions is not Running!"]; };
if (!isnil("AmberMissionsInstalled")) exitWith { diag_log text format ["[AmberMissions]: <ERROR> AmberMissions is Installed Twice or Installed Incorrectly!"]; };
AmberMissionsInstalled = true;

diag_log text format ["[AmberMissions]: Starting DayZ Mission System."];

// Let's see if we need to set relationships

if ( (isnil("DZAI_isActive")) && (isnil("SAR_version")) && (isnil("WAIconfigloaded")) ) then
{
	diag_log text format ["[AmberMissions]: Relations not found! Using AmberMissions Relations."];
	
	createCenter east;
	// Make AI Hostile to Survivors
	WEST setFriend [EAST,0];
	EAST setFriend [WEST,0];
	// Make AI Hostile to Zeds
	EAST setFriend [CIVILIAN,0];
	CIVILIAN setFriend [EAST,0];
	
} else {

	AmberMissionsRelations = 0;
	if (!isnil("DZAI_isActive")) then {
		diag_log text format ["[AmberMissions]: DZAI Found! Using DZAI's Relations!"];
		AmberMissionsRelations = AmberMissionsRelations + 1;
	};
	if (!isnil("SAR_version")) then {
		diag_log text format ["[AmberMissions]: SargeAI Found! Using SargeAI's Relations!"];
		AmberMissionsRelations = AmberMissionsRelations + 1;
	};
	if (!isnil("WAIconfigloaded")) then {
		diag_log text format ["[AmberMissions]: WickedAI Found! Using WickedAI's Relations!"];
		AmberMissionsRelations = AmberMissionsRelations + 1;
	};
	if (AmberMissionsRelations > 1) then {
		diag_log text format ["[AmberMissions]: Multiple Relations Detected! Unwanted AI Behaviour May Occur!"];
		diag_log text format ["[AmberMissions]: If Issues Arise, Decide on a Single AI System! (DayZAI, SargeAI, or WickedAI)"];
	};
	AmberMissionsRelations = nil;
	
};

call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\AmberMissionsConfig.sqf";


AmberMissionsWorldName = toLower format ["%1", worldName];
diag_log text format ["[AmberMissions]: %1 Detected. Map Specific Settings Adjusted!", AmberMissionsWorldName];

// Detect Epoch 
_modVariant = toLower( getText (configFile >> "CfgMods" >> "DayZ" >> "dir"));
if (_modVariant == "@dayz_epoch") then {AmberMissionsEpoch = true;} else {AmberMissionsEpoch = false;};
if ((!(AmberMissionsEpoch)) AND (!(isNil "PVDZE_serverObjectMonitor"))) then {AmberMissionsEpoch = true;};

if (AmberMissionsEpoch) then {
		diag_log text format ["[AmberMissions]: DayZ Epoch Detected! Some Scripts Adjusted!"];
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsEpochWeaponCrateList.sqf";
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsEpochAIConfig.sqf";
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsEpochVehicles.sqf";
		AmberMissionsMajorArray = ["SM1","SM2","SM3","SM4","SM5","SM6","SM7","SM8","SM9","SM10","SM11","SM12","SM13","EPOCH1"];
		AmberMissionsMinorArray = ["SM1","SM2","SM3","SM4","SM5","SM6","SM7","SM8","SM9","SM10","SM11","SM12","SM13"];
		diag_log text format ["[AmberMissions]: Epoch Mission and Extended Configuration Loaded!"];
	} else {
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsWeaponCrateList.sqf";
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsAIConfig.sqf";
		call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\ExtraConfig\AmberMissionsVehicles.sqf";
		AmberMissionsMajorArray = ["SM1","SM2","SM3","SM4","SM5","SM6","SM7","SM8","SM9","SM10","SM11","SM12","SM13"];
		AmberMissionsMinorArray = ["SM1","SM2","SM3","SM4","SM5","SM6","SM7","SM8","SM9","SM10","SM11","SM12","SM13"];
		diag_log text format ["[AmberMissions]: Mission and Extended Configuration Loaded!"];
	};

call compile preprocessFileLineNumbers "\z\addons\dayz_server\AmberMissions\AmberMissionsFunctions.sqf";
AmberMissionsUnitsMinor = [];
AmberMissionsUnitsMajor = [];
[] ExecVM AmberMissionsMajTimer;
[] ExecVM AmberMissionsMinTimer;
[] ExecVM AmberMissionsMarkerLoop;
