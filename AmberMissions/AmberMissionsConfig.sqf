/*
	DayZ Mission System Config by AmberHelios 2014
	You can adjust the weapons that spawn in weapon crates inside AmberMissionsWeaponCrateList.sqf
	And the AI's gear inside AmberMissionsAIConfig.sqf in the ExtraConfig folder.
*/

// Humanity gain from killing mission AI
AmberMissionsMissHumanity = true;

// How Much Humanity?
AmberMissionsCntHumanity = 150;

// Do you want AI kills to count as bandit kills?
AmberMissionsCntBanditKls = true;

//Do you want AI to disappear instantly when killed?
AmberMissionsCleanDeath = false;

 // Remove Gear from AI that are run over
AmberMissionsRunGear = true;

// Body Clean-up
AmberMissionsBodyTime = 2400;

// Do You Want AI to use NVGs? (They are deleted on death)
AmberMissionsUseNVG = true;

// Do you want vehicles from missions to save to the Database?
AmberMissionsSaveVehicles = false;

//Major Marker Size
AmberMissionsMajorSize = 175;

//Minor Marker Size
AmberMissionsMinorSize = 150;

// The Minimum time in seconds before a major mission will run.
AmberMissionsMajorMin = 600;

// Maximum time in seconds before a major mission will run.
AmberMissionsMajorMax = 1800;

// The Minimum time in seconds before a minor mission will run.
AmberMissionsMinorMin = 420;

// Maximum time in seconds before a minor mission will run.
AmberMissionsMinorMax = 900;

// Percentage of AI that must be dead before mission completes (default = 0)
AmberMissionsRequiredKillPercent = 0;

// How long in seconds before mission scenery disappears
AmberMissionsSceneryDespawnTimer = 1800;
 
// Should crates de spawn with scenery
AmberMissionsSceneryDespawnLoot = false;

// Blacklist Zone Array -- missions will not spawn in these areas format: [[x,y,z],radius]
 AmberMissionsBlacklistZones = [[[0,0,0],50]];
