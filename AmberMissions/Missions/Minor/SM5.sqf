/*
	Hummer Wreck by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crash","_crate"];

_Name = "Bandit Humvee Crash";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A Humvee has crashed! Check your map for the location!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//CrashSite
_crash = createVehicle ["HMMWVwreck",_coords,[], 0, "CAN_COLLIDE"];
[_crash] call AmberMissionsProtectObj;

// Crates
_crate = createVehicle ["RULaunchersBox",[(_coords select 0) - 14, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;

// AI 
[_coords,3,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The crash site has been secured by survivors!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM5 Humvee Crash Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;