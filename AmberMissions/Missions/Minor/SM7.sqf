/*  Sniper team script Created by TheSzerdi Edited by Falcyn [QF]
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords"];

_Name = "Sniper team";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Snipers are planning an ambush! Pay attention!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;


//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

[_coords,3,3,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,3,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The snipers have been neutralized!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM7 Sniper Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;
