/*
	Angry Axemans by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords"];

_Name = "Angry Axeman's";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Angry Axemen are on the loose! Watch out before they catch you!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,6,4,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp; 
[nil,nil,rTitleText,"The axemen were chopped down!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM13 Psycho's Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;