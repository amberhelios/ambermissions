/*
	Bandit Hunting Party by lazyink (Full credit to TheSzerdi & TAW_Tonic for the code)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_vehicle","_veh"];

_Name = "Bandit Hunting Party";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A bandit hunting party has been spotted! Check your map for the location!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//Vehicle
_veh = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_vehicle = createVehicle [_veh,[(_coords select 0) + 10, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;

//AI [position,unitcount,skillLevel,1 for major 0 for minor] [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,2,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,2,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,2,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,2,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The hunting party has been wiped out!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM1 Hunting Party Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;