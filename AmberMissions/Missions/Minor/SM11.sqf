/*
	HMMWV Wreck Mission by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_vehicle"];

_Name = "HMMWV Wreck";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A Humvee has crashed! Kill any survivors and secure the loot!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//vehicle
_vehicle = createVehicle ["HMMWVwreck",_coords,[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,3,2,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,3,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp; 
[_vehicle] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The Humvee has been secured by survivors!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM11 HMMWV Crash Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;