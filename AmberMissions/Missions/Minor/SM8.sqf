/*
	Bandit Party by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crash","_tent"];

_Name = "Bandit Party";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Another bandit party has started!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//CrashSite
_crash = createVehicle ["UralWreck",_coords,[], 0, "CAN_COLLIDE"];
[_crash] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,4,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,4,2,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

//This function is !! Not tested !!
_tent = createVehicle ["TentStorage",[(_coords select 0) +2, (_coords select 1) +5,-0.3],[], 0, "NONE"];
[_tent] call AmberMissionsProtectObj;
sleep 1;

_tent addWeaponCargoGlobal   ["ItemCompass", 2];
_tent addWeaponCargoGlobal   ["ItemGPS", 3];
_tent addWeaponCargoGlobal   ["NVGoggles", 1];
_tent addMagazineCargoGlobal ["FoodCanBakedBeans", 4];
_tent addMagazineCargoGlobal ["ItemBandage", 4];
_tent addMagazineCargoGlobal ["ItemMorphine", 4];
_tent addMagazineCargoGlobal ["ItemPainkiller", 4];
_tent addMagazineCargoGlobal ["ItemAntibiotic", 2];
_tent addWeaponCargoGlobal   ["ItemKnife", 2];
_tent addWeaponCargoGlobal   ["ItemToolbox", 2];
_tent addWeaponCargoGlobal   ["ItemMatches", 2];
_tent addMagazineCargoGlobal ["ItemBloodbag", 2];
_tent addMagazineCargoGlobal ["ItemJerryCan", 2];
_tent addMagazineCargoGlobal ["MP5A5", 2];
_tent addMagazineCargoGlobal ["30Rnd_9x19_MP5", 5];
_tent addMagazineCargoGlobal ["glock17_EP1", 2];
_tent addMagazineCargoGlobal ["17Rnd_9x19_glock17", 4];


[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"Bandit party rushed by survivors!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM8 Bandit Party Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;