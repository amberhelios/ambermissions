/*
	Ural Wreck by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crash"];

_Name = "Ural Wreck";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A truck has crashed! Kill any survivors and secure the loot!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//CrashSite
_crash = createVehicle ["uralwreck", _coords,[], 0, "CAN_COLLIDE"];
[_crash] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,6,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,6,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,3,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The crash site has been secured by survivors!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM12 Ural Crash Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;