/*
	Hillbilly Mission by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_base","_base2","_base3"];

_Name = "Hillbillies";
_coords = call AmberMissionsFindPos;
[nil,nil,rTitleText,"Hillbillies have moved into the area", "PLAIN",10] call RE;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//Base
_base = createVehicle ["land_housev_1i4",[(_coords select 0) +2, (_coords select 1)+5,-0.3],[], 0, "CAN_COLLIDE"];
_base2 = createVehicle ["land_kbud",[(_coords select 0) - 10, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
_base3 = createVehicle ["land_kbud",[(_coords select 0) - 7, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,4,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,6,1,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,6,2,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,4,3,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The Survivors raped the Hillbillies! Loot them!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM9 Hillbilly Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;