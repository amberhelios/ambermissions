/*
	Medical Outpost by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_base","_base1","_base2","_base3","_vehicle","_vehicle1","_crate","_crate2","_veh","_veh2"];

_Name = "Bandit Medical Outpost";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A group of bandits have taken over a Medical Outpost! Check your map for the location!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//Base
_base = createVehicle ["US_WarfareBFieldhHospital_Base_EP1",[(_coords select 0) +2, (_coords select 1)+5,-0.3],[], 0, "CAN_COLLIDE"];
_base1 = createVehicle ["MASH_EP1",[(_coords select 0) - 24, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
_base2 = createVehicle ["MASH_EP1",[(_coords select 0) - 17, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
_base3 = createVehicle ["MASH_EP1",[(_coords select 0) - 10, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_base] call AmberMissionsProtectObj;
[_base1] call AmberMissionsProtectObj;
[_base2] call AmberMissionsProtectObj;
[_base3] call AmberMissionsProtectObj;

//Vehicles
_veh = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_veh2 = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_vehicle = createVehicle [_veh,[(_coords select 0) + 10, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
_vehicle1 = createVehicle [_veh2,[(_coords select 0) + 15, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;
[_vehicle1] call AmberMissionsSetupVehicle;

//Crates
_crate = createVehicle ["USVehicleBox",[(_coords select 0) - 3, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
_crate2 = createVehicle ["USLaunchersBox",[(_coords select 0) - 8, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate2,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate2] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[[(_coords select 0) - 20, (_coords select 1) - 15,0],4,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 3;
[[(_coords select 0) + 10, (_coords select 1) + 15,0],4,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 3;
[[(_coords select 0) - 10, (_coords select 1) - 15,0],4,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 3;
[[(_coords select 0) + 20, (_coords select 1) + 15,0],4,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 3;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[_vehicle1] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The Medical Outpost is under survivor control!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM2 Medical Outpost Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;