/*
	Weapon Truck Crash by lazyink (Full credit for code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crash","_crate","_crate1","_crate2"];

_Name = "Bandit Weapons Truck";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A bandit weapons truck has crashed! Check your map for the location!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMinMarker;

//CrashSite
_crash = createVehicle ["UralWreck",_coords,[], 0, "CAN_COLLIDE"];
[_crash] call AmberMissionsProtectObj;

// Crates
_crate = createVehicle ["USLaunchersBox",[(_coords select 0) + 3, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
_crate1 = createVehicle ["USLaunchersBox",[(_coords select 0) - 3, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate1,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate1] call AmberMissionsProtectObj;
_crate2 = createVehicle ["RULaunchersBox",[(_coords select 0) - 6, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate2,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate2] call AmberMissionsProtectObj;

// AI
[_coords,3,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;
[_coords,3,0,"AmberMissionsUnitsMinor"] call AmberMissionsAISpawn;
sleep 1;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The crash site has been secured by survivors!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Minor SM6 Ural Crash Mission has Ended."];
deleteMarker "AmberMissionsMinMarker";
deleteMarker "AmberMissionsMinDot";

AmberMissionsMinDone = true;