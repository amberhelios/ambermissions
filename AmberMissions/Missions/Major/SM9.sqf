/*
	Fire Base Camp by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crate","_crate2","_base1","_base2"];

_Name = "Fire Base Camp";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A firebase is being constructed! Stop the bandits and secure the construction materials for yourself!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Base
_base1 = createVehicle ["land_fortified_nest_big",[(_coords select 0) - 20, (_coords select 1) - 10,-0.2],[], 0, "CAN_COLLIDE"];
[_base1] call AmberMissionsProtectObj;
_base2 = createVehicle ["Land_Fort_Watchtower",[(_coords select 0) - 10, (_coords select 1) + 10,-0.2],[], 0, "CAN_COLLIDE"];
[_base2] call AmberMissionsProtectObj;

//Crate
_crate = createVehicle ["USLaunchersBox",[(_coords select 0) + 22, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
_crate2 = createVehicle ["USLaunchersBox",[(_coords select 0) + 22, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate2,"supply"] ExecVM AmberMissionsBoxSetup;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,6,2,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,6,2,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;

[_coords,"AmberMissionsUnitsMinor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"Survivors have secured the construction materials.", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM9 Fire Base Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;