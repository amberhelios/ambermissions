/*
	Medical Ural Attack by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked by AmberHelios 2014
*/
private ["_Name","_coords","_crash","_vehicle","_vehicle1","_crate","_crate2","_veh"];

_Name = "Ural Ambush";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Bandits have Ambushed a Ural Carrying Supplies!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Crashsite
_crash = createVehicle ["UralWreck",_coords,[], 0, "CAN_COLLIDE"];
_crash setDir 149.64919;
[_crash] call AmberMissionsProtectObj;
_body = createVehicle ["Body",[(_coords select 0) - 2.2905,(_coords select 1) - 3.3438,0],[], 0, "CAN_COLLIDE"];
_body setDir 61.798588;
[_body] call AmberMissionsProtectObj;
_body1 = createVehicle ["Body",[(_coords select 0) - 2.8511,(_coords select 1) - 2.4346,0],[], 0, "CAN_COLLIDE"];
_body1 setDir 52.402905;
[_body1] call AmberMissionsProtectObj;
_body2 = createVehicle ["Body",[(_coords select 0) - 3.435,(_coords select 1) - 1.4297,0],[], 0, "CAN_COLLIDE"];
_body2 setDir -117.27345;
[_body2] call AmberMissionsProtectObj;
_body3 = createVehicle ["Body2",[(_coords select 0) - 4.0337,(_coords select 1) + 0.5,0],[], 0, "CAN_COLLIDE"];
_body3 setDir 23.664057;
[_body3] call AmberMissionsProtectObj;

//vehicle
_veh = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_vehicle = createVehicle [_veh,[(_coords select 0) + 5.7534, (_coords select 1) - 9.2149,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;

//Crates
_crate = createVehicle ["USBasicWeaponsBox",[(_coords select 0) + 2.6778,(_coords select 1) - 3.0889,0],[], 0, "CAN_COLLIDE"];
[_crate,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
_crate setDir -28.85478;
_crate2 = createVehicle ["USBasicWeaponsBox",[(_coords select 0) + 1.4805,(_coords select 1) - 3.7432,0],[], 0, "CAN_COLLIDE"];
[_crate2,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate2] call AmberMissionsProtectObj;
_crate2 setDir 62.744293;
_crate3 = createVehicle ["USBasicAmmunitionBox",[(_coords select 0) + 2.5405,(_coords select 1) - 4.1612,0],[], 0, "CAN_COLLIDE"];
[_crate3,"supply"] ExecVM AmberMissionsBoxSetup;
[_crate3] call AmberMissionsProtectObj;
_crate3 setDir -27.93351;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[[(_coords select 0) - 6.9458,(_coords select 1) - 3.5352, 0],6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 4.4614,(_coords select 1) + 2.5898, 0],6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 4.4614,(_coords select 1) + 2.5898, 0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

waitUntil{{isPlayer _x && _x distance _coords <= 30  } count playableunits > 0}; 
[_vehicle] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The Ural Supplies have been Secured by Survivors!", "PLAIN",6] call RE;
diag_log text format ["[AmberMissions]: Major SM5 Ural Ambush Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;