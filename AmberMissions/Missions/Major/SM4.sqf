/*
	Bandit Supply Heli Crash by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked by AmberHelios 2014
*/
private ["_Name","_coords","_ranChopper","_chopper","_truck","_trash","_trash2","_crate","_crate2","_veh"];

_Name = "Helicopter Landing";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A Supply Helicopter has been Forced to Land!\nStop the Bandits from Taking Control of it!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Heli
_ranChopper = ["UH1H_DZ","Mi17_DZ"] call BIS_fnc_selectRandom;
_chopper = createVehicle [_ranChopper,_coords,[], 0, "NONE"];
[_chopper] call AmberMissionsSetupVehicle;
_chopper setDir -36.279881;
_veh = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_truck = createVehicle [_veh,[(_coords select 0) - 8.7802,(_coords select 1) + 6.874,0],[], 0, "CAN_COLLIDE"];
[_truck] call AmberMissionsSetupVehicle;

//Trash
_trash = createVehicle ["Body1",[(_coords select 0) - 3.0185,(_coords select 1) - 0.084,0],[], 0, "CAN_COLLIDE"];
_trash2 = createVehicle ["Body2",[(_coords select 0) + 1.9248,(_coords select 1) + 2.1201,0],[], 0, "CAN_COLLIDE"];
[_trash] call AmberMissionsProtectObj;
[_trash2] call AmberMissionsProtectObj;

//Crates
_crate = createVehicle ["USLaunchersBox",[(_coords select 0) - 6.1718,(_coords select 1) + 0.6426,0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
_crate2 = createVehicle ["USLaunchersBox",[(_coords select 0) - 7.1718,(_coords select 1) + 1.6426,0],[], 0, "CAN_COLLIDE"];
[_crate2,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate2] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[[(_coords select 0) - 8.4614,(_coords select 1) - 5.0527,0],6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) - 8.4614,(_coords select 1) - 5.0527,0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 7.5337,(_coords select 1) + 4.2656,0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 7.5337,(_coords select 1) + 4.2656,0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

waitUntil{{isPlayer _x && _x distance _coords <= 30  } count playableunits > 0}; 
[_chopper] ExecVM AmberMissionsSaveVeh;
[_truck] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The Helicopter has been Taken by Survivors!", "PLAIN",6] call RE;
diag_log text format ["[AmberMissions]: Major SM4 Helicopter Landing Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;