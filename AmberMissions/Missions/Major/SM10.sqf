/*
	CH47 Mission by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_vehicle"];

_Name = "CH47 Mission";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A CH47 has crash landed! Secure its firepower for yourself!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//vehicles
_vehicle = createVehicle ["CH_47F_EP1",[(_coords select 0) + 25, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;
	
//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The helicopter has been secured!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM10 CH47 Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;