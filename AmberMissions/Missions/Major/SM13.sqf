/*
	Constructors Mission by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crate","_crate2"];

_Name = "Constructors";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Intel suggests bandits may be trying to build a base nearby; locate and terminate before they get fortified.", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Crate
_crate = createVehicle ["USVehicleBox",[(_coords select 0) + 25, (_coords select 1),0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
_crate2 = createVehicle ["USVehicleBox",[(_coords select 0) + 25, (_coords select 1),0],[], 0, "CAN_COLLIDE"];
[_crate2,"supply"] ExecVM AmberMissionsBoxSetup;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"Survivors have terminated the bandits!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM13 Constructors Mission Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;