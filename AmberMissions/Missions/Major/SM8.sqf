/*																					//
	Weapons Cache Mission by lazyink (Original Full Code by TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_crash","_crate","_crate2","_crate3"];

_Name = "Weapons Cache";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A C-130 has crash landed! Secure its cargo for yourself!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Crashsite
_crash = createVehicle ["C130J_wreck_EP1",[(_coords select 0) + 35, (_coords select 1) - 5,0],[], 0, "NONE"];

//Crates
_crate = createVehicle ["USVehicleBox",[(_coords select 0) - 20, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
_crate2 = createVehicle ["USVehicleBox",[(_coords select 0) + 20, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate2,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate2] call AmberMissionsProtectObj;
_crate3 = createVehicle ["USVehicleBox",[(_coords select 0) + 30, _coords select 1,0],[], 0, "CAN_COLLIDE"];
[_crate3,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate3] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,4,2,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,4,3,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The C-130 crash site has been secured!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM8 C130 Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;