/*
	HMMWV Mission by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_vehicle","_vehicle1"];

_Name = "HMMWV's";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Two HMMWVs have broken down! Secure their firepower for yourself!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//vehicles
_vehicle = createVehicle ["HMMWV_M998_crows_MK19_DES_EP1",[(_coords select 0) + 10, (_coords select 1) - 10,0],[], 0, "CAN_COLLIDE"];
_vehicle1 = createVehicle ["HMMWV_M998_crows_MK19_DES_EP1",[(_coords select 0) + 20, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;
[_vehicle1] call AmberMissionsSetupVehicle;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,5,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,5,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

 [_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[_vehicle1] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"Survivors secured the HMMWVs!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM11 HMMWV Mission Has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;