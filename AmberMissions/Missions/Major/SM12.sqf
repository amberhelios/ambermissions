/*
	Medical Supply Camp by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/
private ["_Name","_coords","_vehicle","_vehicle1","_vehicle2"];

_Name = "APC's broken down!";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Three APCs are broken down! Check your map for the location!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//vehicles
_vehicle = createVehicle ["BTR60_TK_EP1",[(_coords select 0) - 10, (_coords select 1) - 10,0],[], 0, "CAN_COLLIDE"];
_vehicle1 = createVehicle ["LAV25",[(_coords select 0) + 20, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
_vehicle2 = createVehicle ["LAV25_HQ",[(_coords select 0) + 30, (_coords select 1) - 5,0],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;
[_vehicle1] call AmberMissionsSetupVehicle;
[_vehicle2] call AmberMissionsSetupVehicle;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,3,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[_vehicle1] ExecVM AmberMissionsSaveVeh;
[_vehicle2] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"Survivors have taken control of the APCs!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM12 APC Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;