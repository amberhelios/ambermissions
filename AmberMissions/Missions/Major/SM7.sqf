/*																					//
	Weapons Cache Mission by lazyink (Original Full Code by TheSzerdi & TAW_Tonic)
	Reworked  by AmberHelios 2014
*/	
private ["_Name","_coords","_crate"];

_Name = "Large Ammo Cache";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"A gear cache has been airdropped! Secure it for yourself!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Crates
_crate = createVehicle ["USVehicleBox",_coords,[], 0, "CAN_COLLIDE"];
[_crate,"weapons"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[_coords,6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,6,2,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,8,3,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[_coords,8,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[nil,nil,rTitleText,"The gear cache has been found, nice work, enjoy the spoils!", "PLAIN",6] call RE;

diag_log text format ["[AmberMissions]: Major SM7 Weapon Cache Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;