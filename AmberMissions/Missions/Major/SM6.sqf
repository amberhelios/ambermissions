/*
	Medical Crate by lazyink (Full credit for original code to TheSzerdi & TAW_Tonic)
	Reworked by AmberHelios 2014
*/
private ["_Name","_coords","_net","_vehicle","_vehicle1","_crate","_veh","_veh2"];

_Name = "Medical Cache";
_coords = call AmberMissionsFindPos;
diag_log text format ["[AmberMissions]:Mission Running at %1",_coords];
[nil,nil,rTitleText,"Bandits have Taken Over a Survivor Medical Cache!", "PLAIN",10] call RE;
[_coords,_Name] ExecVM AmberMissionsAddMajMarker;

//Netting
_net = createVehicle ["Land_CamoNetB_NATO",[(_coords select 0) - 0.0649, (_coords select 1) + 0.6025,0],[], 0, "CAN_COLLIDE"];
[_net] call AmberMissionsProtectObj;

//vehicles
_veh = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_veh2 = AmberMissionsLandVecicleList call BIS_fnc_selectRandom;
_vehicle = createVehicle [_veh,[(_coords select 0) + 10.0303, (_coords select 1) - 12.2979,10],[], 0, "CAN_COLLIDE"];
_vehicle1 = createVehicle [_veh2,[(_coords select 0) - 6.2764, (_coords select 1) - 14.086,10],[], 0, "CAN_COLLIDE"];
[_vehicle] call AmberMissionsSetupVehicle;
[_vehicle1] call AmberMissionsSetupVehicle;

//Crates
_crate = createVehicle ["USVehicleBox",_coords,[], 0, "CAN_COLLIDE"];
_crate1 = createVehicle ["MedBox0",[(_coords select 0) - 3.7251,(_coords select 1) - 2.3614, 0],[], 0, "CAN_COLLIDE"];
_crate2 = createVehicle ["MedBox0",[(_coords select 0) - 3.4346, 0, 0],[], 0, "CAN_COLLIDE"];
_crate3 = createVehicle ["MedBox0",[(_coords select 0) + 4.0996,(_coords select 1) + 3.9072, 0],[], 0, "CAN_COLLIDE"];
[_crate,"medical"] ExecVM AmberMissionsBoxSetup;
[_crate] call AmberMissionsProtectObj;
[_crate1] call AmberMissionsProtectObj;
[_crate2] call AmberMissionsProtectObj;
[_crate3] call AmberMissionsProtectObj;

//AI [position,unitcount,skillLevel,1 for major 0 for minor]
[[(_coords select 0) + 0.0352,(_coords select 1) - 6.8799, 0],6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 0.0352,(_coords select 1) - 6.8799, 0],6,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 0.0352,(_coords select 1) - 6.8799, 0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;
[[(_coords select 0) + 0.0352,(_coords select 1) - 6.8799, 0],4,1,"AmberMissionsUnitsMajor"] call AmberMissionsAISpawn;
sleep 5;

[_coords,"AmberMissionsUnitsMajor"] call AmberMissionsWaitMissionComp;
[_vehicle] ExecVM AmberMissionsSaveVeh;
[_vehicle1] ExecVM AmberMissionsSaveVeh;
[nil,nil,rTitleText,"The Medical Cache is Under Survivor Control!", "PLAIN",6] call RE;
diag_log text format ["[AmberMissions]: Major SM6 Medical Cache Mission has Ended."];
deleteMarker "AmberMissionsMajMarker";
deleteMarker "AmberMissionsMajDot";

AmberMissionsMajDone = true;