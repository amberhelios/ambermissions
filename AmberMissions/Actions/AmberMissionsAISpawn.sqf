/*																		//
	AmberMissionsAISpawn.sqf by AmberHelios 2014
	Usage: [position,unitcount,skillLevel,MissionType] execVM ""\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsAISpawn.sqf";
		Position is the coordinates to spawn at [X,Y,Z]
		UnitCount is the number of units to spawn
		SkillLevel is the skill number defined in AmberMissionsAIConfig.sqf in the ExtraConfig folder
		MissionType 1 for major 0 for minor
*/																		//
private ["_position","_unitcount","_skill","_wpRadius","_xpos","_ypos","_unitGroup","_aiskin","_unit","_weapon","_magazine","_wppos1","_wppos2","_wppos3","_wppos4","_wp1","_wp2","_wp3","_wp4","_wpfin","_unitArrayName"];
_position = _this select 0;
_unitcount = _this select 1;
_skill = _this select 2;
_unitArrayName = _this select 3;

//diag_log text format ["[AmberMissions]: AI Pos:%1 / AI UnitNum: %2 / AI SkillLev:%3",_position,_unitcount,_skill];

_wpRadius = 20;

_xpos = _position select 0;
_ypos = _position select 1;
_unitGroup = createGroup east;
if (!isServer) exitWith {};

for "_x" from 1 to _unitcount do {

	switch (_skill) do {
		case 0: {_aiskin = AmberMissionsBanditSkins call BIS_fnc_selectRandom;};
		case 1: {_aiskin = AmberMissionsBanditSkins call BIS_fnc_selectRandom;};
		case 2: {_aiskin = AmberMissionsBanditSkins call BIS_fnc_selectRandom;};
		case 3: {_aiskin = AmberMissionsBanditSkins call BIS_fnc_selectRandom;};
		case 4: {_aiskin = AmberMissionsBanditSkins call BIS_fnc_selectRandom;};
		case 5: {_aiskin = AmberMissionsBansheeSkins1 call BIS_fnc_selectRandom;};
		case 6: {_aiskin = AmberMissionsBansheeSkins2 call BIS_fnc_selectRandom;};
	};
	_unit = _unitGroup createUnit [_aiskin, [(_position select 0),(_position select 1),(_position select 2)], [], 10, "PRIVATE"];
	[_unit] joinSilent _unitGroup;
	_unit enableAI "TARGET";
	_unit enableAI "AUTOTARGET";
	_unit enableAI "MOVE";
	_unit enableAI "ANIM";
	_unit enableAI "FSM";
	_unit setCombatMode "YELLOW";
	_unit setBehaviour "COMBAT";
	removeAllWeapons _unit;
	removeAllItems _unit;
	//Get the weapon array based on skill
	_weaponArray = [_skill] call AmberMissionsGetWeapon;
	_weapon = _weaponArray select 0;
	_magazine = _weaponArray select 1;
	
//diag_log text format ["[AmberMissions]: AI Weapon:%1 / AI Magazine:%2",_weapon,_magazine];
	switch (_skill) do {
		case 0: {_aigearArray = [AmberMissionsGear0,AmberMissionsGear1,AmberMissionsGear2,AmberMissionsGear3,AmberMissionsGear4];};
		case 1: {_aigearArray = [AmberMissionsGear0,AmberMissionsGear1,AmberMissionsGear2,AmberMissionsGear3,AmberMissionsGear4];};
		case 2: {_aigearArray = [AmberMissionsGear0,AmberMissionsGear1,AmberMissionsGear2,AmberMissionsGear3,AmberMissionsGear4];};
		case 3: {_aigearArray = [AmberMissionsGear0,AmberMissionsGear1,AmberMissionsGear2,AmberMissionsGear3,AmberMissionsGear4];};
		case 4: {_aigearArray = [AmberMissionsGear5,AmberMissionsGear6];};
		case 5: {_aigearArray = [AmberMissionsGear7,AmberMissionsGear8];};
		case 6: {_aigearArray = [AmberMissionsGear7,AmberMissionsGear8];};
	};
	_aigear = _aigearArray call BIS_fnc_selectRandom;
	_gearmagazines = _aigear select 0;
	_geartools = _aigear select 1;
	_aipack = AmberMissionsPacklist call BIS_fnc_selectRandom;
	if (AmberMissionsUseNVG) then {
		_unit addWeapon "NVGoggles";
	};
	for "_i" from 1 to 3 do {
		_unit addMagazine _magazine;
	};
	_unit addWeapon _weapon;
	_unit selectWeapon _weapon;
	
	_unit addBackpack _aipack;
	
	{
		_unit addMagazine _x
	} forEach _gearmagazines;
	
	{
		_unit addWeapon _x
	} forEach _geartools;
	switch (_skill) do {
		case 0: {_aicskill = AmberMissionsSkills1;};
		case 1: {_aicskill = AmberMissionsSkills2;};
		case 2: {_aicskill = AmberMissionsSkills3;};
		case 3: {_aicskill = AmberMissionsSkills4;};
		case 4: {_aicskill = AmberMissionsSkills4;};
		case 5: {_aicskill = AmberMissionsSkills4;};
		case 6: {_aicskill = AmberMissionsSkills4;};
	};
	
	{
		_unit setSkill [(_x select 0),(_x select 1)]
	} forEach _aicskill;
	
	_unit addEventHandler ["Killed",{ [(_this select 0), (_this select 1)] ExecVM AmberMissionsAIKilled; }];
	_unit setVariable ["AmberMissionsAI", true];
};

_wppos1 = [_xpos, _ypos+20, 0];
_wppos2 = [_xpos+20, _ypos, 0];
_wppos3 = [_xpos, _ypos-20, 0];
_wppos4 = [_xpos-20, _ypos, 0];

_wp1 = _unitGroup addWaypoint [_wppos1, _wpRadius];
_wp1 setWaypointType "MOVE";
_wp2 = _unitGroup addWaypoint [_wppos2, _wpRadius];
_wp2 setWaypointType "MOVE";
_wp3 = _unitGroup addWaypoint [_wppos3, _wpRadius];
_wp3 setWaypointType "MOVE";
_wp4 = _unitGroup addWaypoint [_wppos4, _wpRadius];
_wp4 setWaypointType "MOVE";

_wpfin = _unitGroup addWaypoint [[_xpos,_ypos, 0], _wpRadius];
_wpfin setWaypointType "CYCLE";

//diag_log text format ["[AmberMissions]: Spawned %1 AI at %2",_unitcount,_position];
// load the unit groups into a passed array name so they can be cleaned up later
 call compile format["
 %1 = %1 + (units _unitGroup); 
 _unitMissionCount = count %1;
 ",_unitArrayName];
 diag_log text format ["[AmberMissions]: (%3) %1 AI Spawned, %2 units in mission.",count (units _unitGroup),_unitMissionCount,_unitArrayName];