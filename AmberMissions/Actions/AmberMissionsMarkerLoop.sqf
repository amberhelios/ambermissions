/*
	Marker Re setter by AmberHelios 2014
	Marker Re setter checks if a Mission is running and resets the marker for JIPs
*/
private["_run","_nul","_nil"];

diag_log text format ["[AmberMissions]: Mission Marker Loop for JIPs Starting!"];
if (isNil "AmberMissionsMajCoords")then{AmberMissionsMajCoords = [0,0,0];};
if (isNil "AmberMissionsMinCoords")then{AmberMissionsMinCoords = [0,0,0];};
_run = true;
while {_run} do
{
	[25,5] call AmberMissionsSleep;
	if (!(getMarkerColor "AmberMissionsMajMarker" == "")) then {
		deleteMarker "AmberMissionsMajMarker";
		deleteMarker "AmberMissionsMajDot";
		_nul = createMarker ["AmberMissionsMajMarker", AmberMissionsMajCoords];
		"AmberMissionsMajMarker" setMarkerColor "ColorRed";
		"AmberMissionsMajMarker" setMarkerShape "ELLIPSE";
		"AmberMissionsMajMarker" setMarkerBrush "Grid";
		"AmberMissionsMajMarker" setMarkerSize [AmberMissionsMajorSize,AmberMissionsMajorSize];
		_zap = createMarker ["AmberMissionsMajDot", AmberMissionsMajCoords];
		"AmberMissionsMajDot" setMarkerColor "ColorBlack";
		"AmberMissionsMajDot" setMarkerType "Vehicle";
		"AmberMissionsMajDot" setMarkerText AmberMissionsMajName;
	};
	if (!(getMarkerColor "AmberMissionsMinMarker" == "")) then {
		deleteMarker "AmberMissionsMinMarker";
		deleteMarker "AmberMissionsMinDot";
		_nil = createMarker ["AmberMissionsMinMarker", AmberMissionsMinCoords];
		"AmberMissionsMinMarker" setMarkerColor "ColorRed";
		"AmberMissionsMinMarker" setMarkerShape "ELLIPSE";
		"AmberMissionsMinMarker" setMarkerBrush "Grid";
		"AmberMissionsMinMarker" setMarkerSize [AmberMissionsMinorSize,AmberMissionsMinorSize];
		_zip = createMarker ["AmberMissionsMinDot", AmberMissionsMinCoords];
		"AmberMissionsMinDot" setMarkerColor "ColorBlack";
		"AmberMissionsMinDot" setMarkerType "Vehicle";
		"AmberMissionsMinDot" setMarkerText AmberMissionsMinName;
	};
};