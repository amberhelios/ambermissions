/*
	DayZ Mission System Timer by AmberHelios 2014
	Based on fnc_hTime by TAW_Tonic and SMFinder by Craig
*/
private["_run","_timeDiff","_timeVar","_wait","_cntMis","_ranMis","_varName"];

_timeDiff = AmberMissionsMinorMax - AmberMissionsMinorMin;
_timeVar = _timeDiff + AmberMissionsMinorMin;

diag_log text format ["[AmberMissions]: Minor Mission Clock Starting!"];

_run = true;
while {_run} do
{
	_wait  = round(random _timeVar);
	[_wait,5] call AmberMissionsSleep;
	_cntMis = count AmberMissionsMinorArray;
	if (_cntMis == 0) then { _run = false; };
	
	_ranMis = floor (random _cntMis);
	_varName = AmberMissionsMinorArray select _ranMis;
    
    // clean up all the existing units before starting a new one
	{if (alive _x) then {_x call AmberMissionsPurgeObject;};} forEach AmberMissionsUnitsMinor;
    
    // rebuild the array for the next mission
    AmberMissionsUnitsMinor= [];
	
	[] execVM format ["\z\addons\dayz_server\AmberMissions\Missions\Minor\%1.sqf",_varName];
	diag_log text format ["[AmberMissions]: Running Minor Mission %1.",_varName];
	
	waitUntil {AmberMissionsMinDone};
	AmberMissionsMinDone = nil;
};