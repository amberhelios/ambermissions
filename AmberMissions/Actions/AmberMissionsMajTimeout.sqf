/*
	DayZ Mission System Timeout by AmberHelios
*/
private["_run","_missTime","_coords"];

_coords =_this select 0;
_missTime = floor(time);
_run = true;
while {_run} do
{
  sleep 5;
  { if(isPlayer _x && _x distance _coords <= AmberMissionsMajorSize) then { _run = false; }; } forEach playableUnits;
  if (floor(time) - _missTime >= AmberMissionsMajorTimeout) then {
    diag_log format["[AmberMissions]: Major Mission Timeout."];
    AmberMissionsMajDone = true;
    _run = false;
  };
};