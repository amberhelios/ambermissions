/*
	DayZ Mission System Timeout by AmberHelios
*/
private["_run","_missTime","_coords"];

_coords =_this select 0;
_missTime = floor(time);
_run = true;
while {_run} do
{
  sleep 5;
  { if(isPlayer _x && _x distance _coords <= AmberMissionsMinorSize) then { _run = false; }; } forEach playableUnits;
  if (floor(time) - _missTime >= AmberMissionsMinorTimeout) then {
    diag_log format["[AmberMissions]: Minor Mission Timeout."];
    AmberMissionsMinDone = true;
    _run = false;
  };
};