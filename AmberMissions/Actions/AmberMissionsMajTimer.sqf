/*
	DayZ Mission System Timer by AmberHelios 2014
	Based on fnc_hTime by TAW_Tonic and SMFinder by Craig
*/
private["_run","_timeDiff","_timeVar","_wait","_cntMis","_ranMis","_varName"];

_timeDiff = AmberMissionsMajorMax - AmberMissionsMajorMin;
_timeVar = _timeDiff + AmberMissionsMajorMin;

diag_log text format ["[AmberMissions]: Major Mission Clock Starting!"];

_run = true;
while {_run} do
{
	_wait  = round(random _timeVar);
	[_wait,5] call AmberMissionsSleep;
	_cntMis = count AmberMissionsMajorArray;
	if (_cntMis == 0) then { _run = false; };
	
	_ranMis = floor (random _cntMis);
	_varName = AmberMissionsMajorArray select _ranMis;
   
    // clean up all the existing units before starting a new one
	{if (alive _x) then {_x call AmberMissionsPurgeObject;};} forEach AmberMissionsUnitsMajor;
     
     // rebuild the array for the next mission
    AmberMissionsUnitsMajor = [];
	
	[] execVM format ["\z\addons\dayz_server\AmberMissions\Missions\Major\%1.sqf",_varName];
	diag_log text format ["[AmberMissions]: Running Major Mission %1.",_varName];

	waitUntil {AmberMissionsMajDone};
	AmberMissionsMajDone = nil;
};