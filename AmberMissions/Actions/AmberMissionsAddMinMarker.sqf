/*
	Adds a marker for Major Missions. Only runs once.
	AmberMissionsMarkerLoop.sqf keeps this marker updated.
	Usage: [coordinates,missionname]
*/
private["_nul","_nil"];
AmberMissionsMinCoords = _this select 0;
AmberMissionsMinName = _this select 1;

_nul = createMarker ["AmberMissionsMinMarker", AmberMissionsMinCoords];
"AmberMissionsMinMarker" setMarkerColor "ColorRed";
"AmberMissionsMinMarker" setMarkerShape "ELLIPSE";
"AmberMissionsMinMarker" setMarkerBrush "Grid";
"AmberMissionsMinMarker" setMarkerSize [AmberMissionsMinorSize,AmberMissionsMinorSize];
_nil = createMarker ["AmberMissionsMinDot", AmberMissionsMinCoords];
"AmberMissionsMinDot" setMarkerColor "ColorBlack";
"AmberMissionsMinDot" setMarkerType "Vehicle";
"AmberMissionsMinDot" setMarkerText AmberMissionsMinName;