/*
	Adds a marker for Major Missions. Only runs once.
	AmberMissionsMarkerLoop.sqf keeps this marker updated.
	Usage: [coordinates,missionname]
*/
private["_nul","_nil"];
AmberMissionsMajCoords = _this select 0;
AmberMissionsMajName = _this select 1;

_nul = createMarker ["AmberMissionsMajMarker", AmberMissionsMajCoords];
"AmberMissionsMajMarker" setMarkerColor "ColorRed";
"AmberMissionsMajMarker" setMarkerShape "ELLIPSE";
"AmberMissionsMajMarker" setMarkerBrush "Grid";
"AmberMissionsMajMarker" setMarkerSize [AmberMissionsMajorSize,AmberMissionsMajorSize];
_nil = createMarker ["AmberMissionsMajDot", AmberMissionsMajCoords];
"AmberMissionsMajDot" setMarkerColor "ColorBlack";
"AmberMissionsMajDot" setMarkerType "Vehicle";
"AmberMissionsMajDot" setMarkerText AmberMissionsMajName;