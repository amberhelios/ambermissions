/*
	Usage: [_crate,"type"] execVM "\z\addons\dayz_server\AmberMissions\Actions\AmberMissionsBox.sqf";
		_crate is the crate to fill
		"type" is the type of crate
		"type" can be weapons or medical
*/
_crate = _this select 0;
_type = _this select 1;
clearWeaponCargoGlobal _crate;
clearMagazineCargoGlobal _crate;


// Medical Crates
if (_type == "medical") then {
	_scount = count AmberMissionsMedicalList;
	for "_x" from 0 to 20 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsMedicalList select _sSelect;
		_crate addMagazineCargoGlobal [_item,(round(random 2))];
	};
};
// Weapon Crates
if (_type == "weapons") then {
	_scount = count AmberMissionsGrenadesList;
	for "_x" from 0 to 2 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsGrenadesList select _sSelect;
		_crate addMagazineCargoGlobal [_item,(round(random 2))];
	};
	// load packs
	_scount = count AmberMissionsBackpackList;
	for "_x" from 0 to 3 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsBackpackList select _sSelect;
		_crate addBackpackCargoGlobal [_item,1];
	};
	// load pistols
	_scount = count AmberMissionspistolList;
	for "_x" from 0 to 2 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionspistolList select _sSelect;
		_crate addWeaponCargoGlobal [_item,1];
		_ammo = [] + getArray (configFile >> "cfgWeapons" >> _item >> "magazines");
		if (count _ammo > 0) then {
			_crate addMagazineCargoGlobal [(_ammo select 0),(round(random 8))];
		};
	};
	//load sniper
	_scount = count AmberMissionssniperList;
	for "_x" from 0 to 1 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionssniperList select _sSelect;
		_crate addWeaponCargoGlobal [_item,1];
		_ammo = [] + getArray (configFile >> "cfgWeapons" >> _item >> "magazines");
		if (count _ammo > 0) then {
			_crate addMagazineCargoGlobal [(_ammo select 0),(round(random 8))];
		};
	};
	//load magazines
	_scount = count AmberMissionsmgList;
	for "_x" from 0 to 2 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsmgList select _sSelect;
		_crate addWeaponCargoGlobal [_item,1];
		_ammo = [] + getArray (configFile >> "cfgWeapons" >> _item >> "magazines");
		if (count _ammo > 0) then {
			_crate addMagazineCargoGlobal [(_ammo select 0),(round(random 8))];
		};
	};
	//load primary
	_scount = count AmberMissionsprimaryList;
	for "_x" from 0 to 3 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsprimaryList select _sSelect;
		_crate addWeaponCargoGlobal [_item,1];
		_ammo = [] + getArray (configFile >> "cfgWeapons" >> _item >> "magazines");
		if (count _ammo > 0) then {
			_crate addMagazineCargoGlobal [(_ammo select 0),(round(random 8))];
		};
	};
};

// Epoch Supply Crates
if (_type == "supply") then {
	// load tools
	_scount = count AmberMissionsConTools;
	for "_x" from 0 to 2 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsConTools select _sSelect;
		_crate addWeaponCargoGlobal [_item, 1];
	};
	// load construction
	_scount = count AmberMissionsConSupply;
	for "_x" from 0 to 30 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsConSupply select _sSelect;
		_crate addMagazineCargoGlobal [_item,(round(random 2))];
	};
	// load money
	_scount = count AmberMissionsMonSupply;
	for "_x" from 0 to 5 do {
		_sSelect = floor(random _sCount);
		_item = AmberMissionsMonSupply select _sSelect;
		_crate addMagazineCargoGlobal [_item,1];
	};
};