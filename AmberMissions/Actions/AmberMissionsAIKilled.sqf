/*
	AmberMissionsAIKilled.sqf by AmberHelios 2014
*/

private ["_unit","_player","_humanity","_banditkills"];
_unit = _this select 0;
_player = _this select 1;

if (isPlayer _player) then {
	private ["_banditkills","_humanity"];
	
	//diag_log text format ["[AmberMissions]: Debug: Unit killed by %1 at %2", _player, mapGridPosition _unit];
	_humanity = _player getVariable ["humanity",0];
	_banditkills = _player getVariable ["banditKills",0];
	if (AmberMissionsMissHumanity) then {
		_player setVariable ["humanity",(_humanity + AmberMissionsCntHumanity),true];
	};
	if (AmberMissionsCntBanditKls) then {
		_player setVariable ["banditKills",(_banditkills + 1),true];
	};
	
	//Lets inform the nearby AI of roughly the players position
	{
		if (((position _x) distance (position _unit)) <= 300) then {
			_x reveal [_player, 4.0];
		}
	} forEach allUnits;
	
} else {

	//diag_log text format ["[AmberMissions]: Debug: Unit killed by %1 at %2", _player, mapGridPosition _unit];
	if (AmberMissionsRunGear) then {
		//Since a player ran them over, Give them nothing
		removeBackpack _unit;
		removeAllWeapons _unit;
		{
			_unit removeMagazine _x
		} forEach magazines _unit;
	};
	
};

if (AmberMissionsUseNVG) then {
 	_unit removeWeapon "NVGoggles";
 };
 
if (AmberMissionsCleanDeath) then {
	_unit call AmberMissionsPurgeObject;
	if (AmberMissionsCleanDeath) exitWith {};
};
if (AmberMissionsUseNVG) then {
 	_unit removeWeapon "NVGoggles";
 };
//Dead body timer and clean-up
[AmberMissionsBodyTime,10] call AmberMissionsSleep;
_unit call AmberMissionsPurgeObject;