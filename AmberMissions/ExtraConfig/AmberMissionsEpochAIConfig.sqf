
AmberMissionsBanditSkins = ["BanditW2_DZ","Soldier1_DZ","GUE_Soldier_MG_DZ","GUE_Soldier_Sniper_DZ","GUE_Soldier_Crew_DZ","GUE_Soldier_2_DZ","RU_Policeman_DZ","Pilot_EP1_DZ","Haris_Press_EP1_DZ","Ins_Soldier_GL_DZ","GUE_Commander_DZ","Functionary1_EP1_DZ","Priest_DZ","Rocker1_DZ","Rocker2_DZ","Rocker3_DZ","Rocker4_DZ","TK_INS_Warlord_EP1_DZ","TK_INS_Soldier_EP1_DZ","Soldier_Sniper_PMC_DZ","Soldier_TL_PMC_DZ","FR_OHara_DZ","FR_Rodriguez_DZ","CZ_Soldier_Sniper_EP1_DZ","Graves_Light_DZ","Bandit2_DZ","SurvivorWcombat_DZ"];
AmberMissionsBansheeSkins1 = ["Priest_DZ", "Priest_DZ"]; //Skill Level 5
AmberMissionsBansheeSkins2 = ["Priest_DZ", "Priest_DZ"]; //Skill Level 6
AmberMissionsSkills1 = [
["aimingAccuracy",0.60],
["aimingShake",0.60],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills2 = [
["aimingAccuracy",0.15],
["aimingShake",0.20],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills3 = [
["aimingAccuracy",0.60],
["aimingShake",0.60],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills4 = [
["aimingAccuracy",1.00],
["aimingShake",1.00],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsWeps1 = [
"M16A2",
"M16A2GL",
"AK_74",
"M4A1_Aim",
"AKS_74_kobra",
"AKS_74_U",
"AK_47_M",
"M24",
"M1014",
"DMR_DZ",
"M4A1",
"M14_EP1",
"Remington870_lamp",
"MP5A5",
"MP5SD",
"M4A3_CCO_EP1",
"SCAR_H_CQC_CCO_SD",
"SCAR_L_CQC_Holo",
"G36_C_SD_eotech",
"m8_tws_sd",
"VSS_Vintorez",
"SCAR_H_STD_EGLM_Spect",
"MG36"
];

AmberMissionsWeps2 = [
"M16A2",
"M16A2GL",
"M249_DZ",
"AK_74",
"M4A1_Aim",
"AKS_74_kobra",
"AKS_74_U",
"AK_47_M",
"M24",
"SVD_CAMO",
"M1014",
"DMR_DZ",
"M4A1",
"M14_EP1",
"Remington870_lamp",
"M240_DZ",
"M4A1_AIM_SD_camo",
"M16A4_ACG",
"Mk_48_DZ",
"M4A3_CCO_EP1",
"Sa58V_RCO_EP1",
"Sa58V_CCO_EP1",
"M40A3",
"Sa58P_EP1",
"Sa58V_EP1",
"VSS_Vintorez",
"SCAR_H_LNG_Sniper"
];

AmberMissionsWeps3 = [
"FN_FAL",
"FN_FAL_ANPVS4",
"Mk_48_DZ",
"M249_DZ",
"BAF_L85A2_RIS_Holo",
"G36C",
"G36C_camo",
"G36A_camo",
"G36K_camo",
"AK_47_M",
"AKS_74_U",
"M14_EP1",
"bizon_silenced",
"DMR_DZ",
"RPK_74",
"m8_SAW",
"AA12_PMC",
"SVD_NSPU_EP1",
"BAF_LRR_scoped",
"M110_NVG_EP1",
"m8_sharpshooter",
"AKS_74_GOSHAWK",
"SCAR_H_LNG_Sniper_SD",
"AKS_GOLD",
"VSS_Vintorez",
"SCAR_H_CQC_CCO_SD"
];
//Snipers ONLY 
AmberMissionsWeps4 = [
"FN_FAL",
"FN_FAL_ANPVS4",
"Huntingrifle",
"M110_NVG_EP1",
"DMR_DZ",
"M4SPR",
"ksvk",
"m107",
"m107_TWS_EP1",
"BAF_AS50_scoped",
"BAF_LRR_scoped",
"M24",
"M40A3",
"SVD_CAMO",
"SVD_NSPU_EP1",
"VSS_vintorez",
"m8_sharpshooter",
"SCAR_H_LNG_Sniper_SD"
];
AmberMissionsWeps5 = [
"MeleeHatchet"
];
AmberMissionsGear0 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear1 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear2 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear3 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear4 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];
//Medical Only
AmberMissionsGear5 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Medical Only
AmberMissionsGear6 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Banshee mission gear
AmberMissionsGear7 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Banshee mission gear
AmberMissionsGear8 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
AmberMissionsPacklist = [
"DZ_Patrol_Pack_EP1",
"DZ_Assault_Pack_EP1",
"DZ_Czech_Vest_Puch",
"DZ_ALICE_Pack_EP1",
"DZ_TK_Assault_Pack_EP1",
"DZ_British_ACU",
"DZ_CivilBackpack_EP1",
"DZ_Backpack_EP1",
"DZ_LargeGunBag_EP1",
"DZ_GunBag_EP1",
"DZ_TerminalPack_EP1"
];