
// Pistols
AmberMissionspistolList = ["Colt1911","glock17_EP1","M9","M9SD","Makarov","MakarovSD","revolver_EP1","UZI_EP1","UZI_SD_EP1","Sa61_EP1","revolver_gold_EP1"];

// Sniper Rifles
AmberMissionssniperList = ["SVD_CAMO","DMR_DZ","huntingrifle","M14_EP1","M24","M40A3","M16A4_ACG","VSS_Vintorez","SCAR_H_LNG_Sniper","SCAR_H_LNG_Sniper_SD","m8_sharpshooter","M110_NVG_EP1","BAF_LRR_scoped_W","SVD_NSPU_EP1","BAF_LRR_scoped","M4SPR","m107","M24_des_EP1","SVD"];

// Light Machine-guns
AmberMissionsmgList = ["M240_DZ","M249_DZ","Mk_48_DZ","bizon_silenced","MP5A5","MP5SD","RPK_74","Bizon","MG36_camo"];

// Primary Rifles
AmberMissionsprimaryList = ["m8_holo_sd","M4A1_HWS_GL_SD_Camo","BAF_L85A2_RIS_SUSAT","BAF_L85A2_UGL_ACOG","BAF_L85A2_RIS_CWS","BAF_L86A2_ACOG","AK_74_GL_kobra","AK_107_GL_pso","AK_47_S","AKS_GOLD","AKS_74_UN_kobra","AKS_74_NSPU","AKS_74_GOSHAWK","m8_compact","m8_holo_sd","SCAR_L_CQC_Holo","SCAR_L_CQC_CCO_SD","SCAR_L_STD_EGLM_RCO","SCAR_L_STD_Mk4CQT","SCAR_L_STD_HOLO","m8_carbineGL","AK_47_M","AK_74","AKS_74_kobra","AKS_74_U","BAF_L85A2_RIS_Holo","bizon_silenced","FN_FAL_ANPVS4","FN_FAL","G36A_camo","G36C_camo","G36C","G36K_camo","M1014","M16A2","M16A2GL","M4A1_AIM_SD_camo","M4A1_Aim","M4A1_HWS_GL_camo","M4A1","M4A3_CCO_EP1","Remington870_lamp","Sa58P_EP1","Sa58V_CCO_EP1","Sa58V_EP1","Sa58V_RCO_EP1","AA12_PMC","m8_carbine_pmc","m8_tws","SCAR_H_STD_EGLM_Spect","SCAR_H_CQC_CCO","SCAR_L_CQC","AK_107_pso","AK_74_GL"];

//Grenades
AmberMissionsGrenadesList = ["HandGrenade_west","FlareGreen_M203","FlareWhite_M203"];

//Backpacks
AmberMissionsBackpackList =["DZ_LargeGunBag_EP1","DZ_GunBag_EP1","DZ_Patrol_Pack_EP1","DZ_Assault_Pack_EP1","DZ_Czech_Vest_Puch","DZ_ALICE_Pack_EP1","DZ_TK_Assault_Pack_EP1","DZ_British_ACU","DZ_CivilBackpack_EP1","DZ_Backpack_EP1"];
//Medical
AmberMissionsMedicalList = ["ItemBandage","ItemMorphine","ItemEpinephrine","ItemPainkiller","ItemWaterbottle","FoodMRE","ItemAntibiotic","ItemBloodbag"];

// Epoch Supplies.
AmberMissionsConTools = ["ChainSaw","ItemToolbox","ItemCrowbar","ItemKnife","ItemEtool","ItemHatchet_DZE","ItemMatchbox_DZE"];
AmberMissionsConSupply = ["ItemSandbag","ItemCanvas","bulk_empty","CinderBlocks","MortarBucket","ItemTankTrap","ItemPole","PartGeneric","PartPlywoodPack","PartPlankPack","ItemTentOld","ItemTentDomed","ItemTentDomed2","ItemSandbag","ItemWire","workbench_kit","ItemGenerator"];
AmberMissionsMonSupply = ["ItemSilverBar","ItemSilverBar10oz","ItemGoldBar","ItemGoldBar10oz"];