
AmberMissionsBanditSkins = ["Survivor2_DZ", "Bandit1_DZ", "Camo1_DZ", "Sniper1_DZ"];
AmberMissionsBansheeSkins1 = ["Priest_DZ", "Priest_DZ"];
AmberMissionsBansheeSkins2 = ["Priest_DZ", "Priest_DZ"];
AmberMissionsSkills1 = [
["aimingAccuracy",0.60],
["aimingShake",0.60],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills2 = [
["aimingAccuracy",0.15],
["aimingShake",0.20],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills3 = [
["aimingAccuracy",0.60],
["aimingShake",0.60],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];

AmberMissionsSkills4 = [
["aimingAccuracy",1.00],
["aimingShake",1.00],
["aimingSpeed",1.00],
["endurance",1.00],
["spotDistance",1.00],
["spotTime",1.00],
["courage",1.00],
["reloadSpeed",1.00],
["commanding",1.00],
["general",1.00]
];
AmberMissionsWeps1 = [
"M16A2",
"M16A2GL",
"AK_74",
"M4A1_Aim",
"AKS_74_kobra",
"AKS_74_U",
"AK_47_M",
"M24",
"M1014",
"DMR_DZ",
"M4A1",
"M14_EP1",
"Remington870_lamp",
"MP5A5",
"MP5SD",
"M4A3_CCO_EP1"
];

AmberMissionsWeps2 = [
"M16A2",
"M16A2GL",
"M249_DZ",
"AK_74",
"M4A1_Aim",
"AKS_74_kobra",
"AKS_74_U",
"AK_47_M",
"M24",
"SVD_CAMO",
"M1014",
"DMR_DZ",
"M4A1",
"M14_EP1",
"Remington870_lamp",
"M240_DZ",
"M4A1_AIM_SD_camo",
"M16A4_ACG",
"M4A1_HWS_GL_camo",
"Mk_48_DZ",
"M4A3_CCO_EP1",
"Sa58V_RCO_EP1",
"Sa58V_CCO_EP1",
"M40A3",
"Sa58P_EP1",
"Sa58V_EP1"
];

AmberMissionsWeps3 = [
"FN_FAL",
"FN_FAL_ANPVS4",
"Mk_48_DZ",
"M249_DZ",
"BAF_L85A2_RIS_Holo",
"G36C",
"G36C_camo",
"G36A_camo",
"G36K_camo",
"AK_47_M",
"AKS_74_U",
"M14_EP1",
"bizon_silenced",
"DMR_DZ",
"RPK_74"
];
//Snipers ONLY 
AmberMissionsWeps4 = [
"FN_FAL_ANPVS4",
"Huntingrifle",
"SVD_des_EP1",
"m107",
"M24_des_EP1",
"SVD_CAMO"
];
AmberMissionsWeps5 = [
"MeleeHatchet"
];
AmberMissionsGear0 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear1 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear2 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear3 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];

AmberMissionsGear4 = [
["ItemBandage","ItemBandage","ItemPainkiller"],
["ItemKnife","ItemFlashlight"]
];
//Medical Only
AmberMissionsGear5 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Medical Only
AmberMissionsGear6 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Banshee mission gear
AmberMissionsGear7 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
//Banshee mission gear
AmberMissionsGear8 = [
["ItemAntibiotic","ItemBloodbag","ItemBloodbag"],
["ItemKnife","ItemFlashlight"]
];
AmberMissionsPacklist = [
"DZ_Patrol_Pack_EP1",
"DZ_Assault_Pack_EP1",
"DZ_Czech_Vest_Puch",
"DZ_ALICE_Pack_EP1",
"DZ_TK_Assault_Pack_EP1",
"DZ_British_ACU",
"DZ_CivilBackpack_EP1",
"DZ_Backpack_EP1"
];