
// Pistols
AmberMissionspistolList = ["Colt1911","glock17_EP1","M9","M9SD","Makarov","MakarovSD","revolver_EP1","UZI_EP1"];

// Sniper Rifles
AmberMissionssniperList = ["SVD_CAMO","DMR_DZ","huntingrifle","M14_EP1","M24","M40A3","M16A4_ACG"];

// Light Machineguns
AmberMissionsmgList = ["M240_DZ","M249_DZ","Mk_48_DZ","bizon_silenced","MP5A5","MP5SD","RPK_74"];

// Primary Rifles
AmberMissionsprimaryList = ["AK_47_M","AK_74","AKS_74_kobra","AKS_74_U","BAF_L85A2_RIS_Holo","bizon_silenced","FN_FAL_ANPVS4","FN_FAL","G36A_camo","G36C_camo","G36C","G36K_camo","M1014","M16A2","M16A2GL","M4A1_AIM_SD_camo","M4A1_Aim","M4A1_HWS_GL_camo","M4A1","M4A3_CCO_EP1","Remington870_lamp","Sa58P_EP1","Sa58V_CCO_EP1","Sa58V_EP1","Sa58V_RCO_EP1"];

//Grenades
AmberMissionsGrenadesList = ["HandGrenade_west","FlareGreen_M203","FlareWhite_M203"];

//Backpacks
AmberMissionsBackpackList =["DZ_Patrol_Pack_EP1","DZ_Assault_Pack_EP1","DZ_Czech_Vest_Puch","DZ_ALICE_Pack_EP1","DZ_TK_Assault_Pack_EP1","DZ_British_ACU","DZ_CivilBackpack_EP1","DZ_Backpack_EP1"];

//Medical
AmberMissionsMedicalList = ["ItemBandage","ItemMorphine","ItemEpinephrine","ItemPainkiller","ItemWaterbottle","FoodMRE","ItemAntibiotic","ItemBloodbag"];

// DayZ Supplies Tools
AmberMissionsConTools = ["ItemToolbox","ItemCrowbar","ItemKnife","ItemEtool","ItemHatchet","ItemMatchbox"];
// DayZ Supplies 
AmberMissionsConSupply = [];
AmberMissionsMonSupply = [];